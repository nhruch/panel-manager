import { PropsWithChildren, useEffect, useRef, useState } from "react";

export type TestComponentProps = {
    count: number;
}

export const TestComponent = (props: PropsWithChildren<TestComponentProps>) => {
    const [value, setValue] = useState(0);
    const counter = useRef(0);

    useEffect(() => {
        counter.current = setInterval(() => {
            setValue((prev) => prev + 1);
        }, 1000);

        return () => clearInterval(counter.current);
    }, []);

    return <div>Component_{props.count}: {value}</div>;
};