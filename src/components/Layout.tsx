import { Link, Outlet } from "react-router-dom";
import { useCache } from "../cache/useCache";
import { TestComponent } from "./TestComponent";

export const Layout = () => {
  const { render, elements, register, setVisible } = useCache();

  const addElement = () => {
    register(<TestComponent count={elements.length} />);
  }

  return (<>
    <div>
      <Link to={"/home"}>Go Home</Link>
      <Link to={"/users"}>Go Users</Link>
      <button onClick={addElement}>Add</button>
      <input type='number' max={elements.length} min={0} onChange={(event) => setVisible(Number(event.target.value))} />
      {render}
      <Outlet />
    </div>
  </>);
};
