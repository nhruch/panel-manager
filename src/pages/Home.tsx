import { useEffect, useRef, useState } from "react";

export const Home = () => {
     const [value, setValue] = useState(0);
    const counter = useRef(0);

    useEffect(() => {
        counter.current = setInterval(() => {
            setValue((prev) => prev + 1);
        }, 1000);

        return () => clearInterval(counter.current);
    }, []);
    return (
        <div>
            <h1>Home</h1>
            <div>Value: {value}</div>
        </div>
    );
}