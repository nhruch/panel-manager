import React, {
  PropsWithChildren,
  useCallback,
  useRef,
  useState,
} from "react";
import { CacheContext, CacheContextType } from "./CacheContext";
import { CacheComponent, CacheComponentRefFunctions } from "./CacheComponent";

export type CacheProviderProps = {
  size?: number;
};

export const CacheProvider = (props: PropsWithChildren<CacheProviderProps>) => {
  const [elements, setElements] = useState<JSX.Element[]>([]);
  const itemsRef = useRef<Array<CacheComponentRefFunctions>>([]);

  const register = (element: JSX.Element) => {
    setElements((prev) => [...prev, element]);
  };

  const render = useCallback(() => {
    return (
      <>
        {elements.map((element, index) => {
          return (
            <CacheComponent
              key={index}
              ref={(r: CacheComponentRefFunctions) => (itemsRef.current[index] = r)}
            >
              {element}
            </CacheComponent>
          );
        })}
      </>
    );
  }, [elements]);

  const setVisible = (index: number) => {
    for(let i = 0; i < itemsRef.current.length; i++) {
        if (i === index) {
            itemsRef.current[i].setVisible(true);
        } else {
            itemsRef.current[i].setVisible(false);
        }
    }
  }

  const value: CacheContextType = {
    register,
    setVisible,
    elements,
    render: render(),
  };

  return (
    <CacheContext.Provider value={value}>
      {props.children}
    </CacheContext.Provider>
  );
};
