import { useContext } from "react";
import { CacheContext } from "./CacheContext";

export const useCache = () => {
    const cache = useContext(CacheContext);
    if (!cache) {
        throw new Error("useCache must be used within a CacheProvider");
    }
    return cache;
}