import { createContext } from "react";

export type CacheContextType = {
    register: (element: JSX.Element) => void;
    elements: JSX.Element[];
    render: JSX.Element;
    setVisible: (index: number) => void;
}

export const CacheContext = createContext<CacheContextType | null>(null)