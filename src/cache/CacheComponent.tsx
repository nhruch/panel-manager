import React, { PropsWithChildren, forwardRef, memo, useImperativeHandle, useRef } from "react";

export type CacheComponentRefFunctions = {
    setVisible: (visible: boolean) => void;
}

export const CacheComponent = memo(forwardRef((props: PropsWithChildren, ref: React.ForwardedRef<CacheComponentRefFunctions>) => {
    const divRef = useRef<HTMLDivElement>(null);

    // Use this to override the ref instead of applying it to a child element.
    // Second value is new valud for the ref.
    useImperativeHandle(ref, () => ({
        setVisible: (visible: boolean) => {
            if (!divRef.current) {
                return;
            }
            divRef.current.style.display = visible ? "block" : "none";
        }
    }), []);

    return (
        <div ref={divRef}>
            {props.children}
        </div>
    )
}));